package com.employee.dao;

import java.util.ArrayList;
import java.util.List;


public class EmployeeImpl implements EmployeeDao {
	
List<Employee> employee;
	
	public EmployeeImpl() {
		employee = new ArrayList<Employee>();
		Employee emp1 = new Employee(1, "Ram","Pokhara", 25);
		Employee emp2 = new Employee(2, "Hari","Pokhara", 20);
		employee.add(emp1);
		employee.add(emp2);
	}

	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		return employee;
	}

	@Override
	public Employee getEmployee(int id) {
		// TODO Auto-generated method stub
		return employee.get(id);
	}

	@Override
	public void updateEmployee(Employee emp){
		// TODO Auto-generated method stub
		employee.get(emp.getId()).setName(emp.getName());
		System.out.println("Employee with id: "+ emp.getId()+" updated from database");
		
	}

	@Override
	public void deleteEmployee(Employee emp) {
		// TODO Auto-generated method stub
		employee.remove(emp.getId());
		System.out.println("Employee with id: "+ emp.getId()+"deleted from database");
		
	}
	
	

}
