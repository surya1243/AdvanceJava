package com.employee.dao;

import java.util.List;


public interface EmployeeDao {
	public List<Employee> getAllEmployees();
	public Employee getEmployee(int id);
	public void updateEmployee(Employee employee);
	public void deleteEmployee(Employee employee);
}
