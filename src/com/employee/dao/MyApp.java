package com.employee.dao;


public class MyApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		EmployeeDao dao = new EmployeeImpl();

	      //print all Employees
	      for (Employee emp : dao.getAllEmployees()) {
	         System.out.println("Employee: [Id : "+emp.getId()+", Name : "+emp.getName()+
	        		 ", Age: "+emp.getAge()+", Address : "+emp.getAddress()+ "]");
	      }
	      
	      //update employee
	      Employee emp = dao.getAllEmployees().get(0);
	      emp.setName("Surya");
	      emp.setAddress("Lamjung");
	      dao.updateEmployee(emp);
	      
	      
	    //print updated Employee
	      dao.getEmployee(0);
	      System.out.println("Employee: [Id : "+emp.getId()+", Name : "+emp.getName()+
	        		 ", Age: "+emp.getAge()+", Address : "+emp.getAddress()+ "]");	
	}
}
