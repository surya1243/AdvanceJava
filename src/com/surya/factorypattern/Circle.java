package com.surya.factorypattern;

public class Circle implements Shape {
	
	public void draw() {
		System.out.println("drawing circle");
	}

}
