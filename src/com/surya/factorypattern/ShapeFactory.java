package com.surya.factorypattern;

public class ShapeFactory {
	Shape s;
	public Shape getShape (String shapeType) {
		
		if(shapeType==null) {
			return null;
		}
		
		if(shapeType.equalsIgnoreCase("CIRCLE")) {
			s= new Circle();
		}
		else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
			s= new Rectangle();
		}
		else if (shapeType.equalsIgnoreCase("SQUARE")) {
			s= new Square();
		}
		return s;
		
	}

}
