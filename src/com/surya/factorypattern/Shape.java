package com.surya.factorypattern;

public interface Shape {
	
	public void draw();

}
