package com.surya.factorypattern;

public class MyApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ShapeFactory shapeFactory = new ShapeFactory();
		
		Shape shape1= shapeFactory.getShape("Rectangle");
		shape1.draw();
		

	}

}
