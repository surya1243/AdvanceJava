package com.surya.mvcpattern;

public class StudentView {
	
	public void printStudentDetails(String name, String roll) {
		
		System.out.println("Name : "+name+" and Roll no: "+roll);
	}

}
