package com.surya.singleton;

public class SingleObject {
	
	public static SingleObject instance = new SingleObject();
	
	//instantiated
	private SingleObject() {}

	//get the only object available
	public static SingleObject getInstance() {
		return instance;
	}
	
	public void showMessage() {
		System.out.println("Hello world");
	}
}
