package com.surya.daopattern;

import java.util.List;
//logical class banaune dao ma
public interface StudentDao {
	
	public List<Student> getAllStudents();
	public Student getStudent(int rollNo);
	public void updateStudent(Student student);
	public void deleteStudent(Student student);

}
