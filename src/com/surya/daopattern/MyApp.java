package com.surya.daopattern;

public class MyApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		StudentDao studentDao = new StudentDaoimpl();
		
		//print all students
		for (Student student: studentDao.getAllStudents()) {
			System.out.println("Student: RollNo:" + student.getRollNo()+", Name: "+student.getName());
		}
		
		//update student
		
		Student student = studentDao.getAllStudents().get(0);
		student.setName("Michale");
		studentDao.updateStudent(student);
		
		//get the student
		studentDao.getStudent(0);
		System.out.println("Student: RollNo:" + student.getRollNo()+", Name: "+student.getName());
	}

}
