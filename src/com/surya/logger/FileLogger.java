package com.surya.logger;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class FileLogger implements Logger {
	public void show() {
		String message = "Hello world!";
		File file = new File("logger.txt");
		
		try {
			FileUtils.writeStringToFile(file, message);
			System.out.println("printing from FileLogger");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
