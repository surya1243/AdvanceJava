package com.surya.logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoggerFactory {
	Logger logger;
	InputStream inputStream;
	public String getfilelog() throws IOException {
		Properties prop = new Properties();
		String propFileName = "logers.properties";
		 
		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		
		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
		
		String file = prop.getProperty("FILE");
		System.out.println(file);
		return file;
	}
	
	
	public Logger checkFile(String file) {
		if(file==null) {
			return null;
		}
		
		if(file.equalsIgnoreCase("TRUE")) {
			logger= new FileLogger();
			
		}
		else {
			logger= new ConsoleLogger();
			
		}
		
		
		return logger;
	}

}
